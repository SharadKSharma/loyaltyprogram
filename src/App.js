import React, { useEffect, useState } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import "./App.css";
import Products from "./components/Products";
import Cart from "./components/Cart";
import Checkout from "./components/Checkout";
import Login from "./components/Login";
import OrderPlace from "./components/OrderPlace";
import History from "./components/History";
import axios from "axios";
import Product1 from "./img/products/Insulin.png";
import Product2 from "./img/products/Panacur.png";
import Product3 from "./img/products/Revolt.png";
import Product4 from "./img/products/Simparica.png";
import Product5 from "./img/products/Terramycin.png";


const App = () => {
  const productDetails = [
    {
      clientRetail: "21.48",
      handling: "15",
      id: "100",
      productName: "Insulin Vial Protector for Lantus 2 Pack",
      subTotal: "36.48",
      img:Product1
    },
    {
      clientRetail: "19.48",
      handling: "12",
      id: "101",
      productName: "Panacur Equine Paste 10% Horse Dewormer, 25g",
      subTotal: "31.48",
      img:Product2
    },
    {
      clientRetail: "25.25",
      handling: "10",
      id: "102",
      productName: "Revolt Topical Solution for Cats, 5.1-15 lbs",
      subTotal: "35.25",
      img:Product3
    },
    {
      clientRetail: "27.18",
      handling: "13",
      id: "103",
      productName: "Simparica Trio Chewable Tablet for Dogs",
      subTotal: "40.48",
      img:Product4
    }
    ,{
      clientRetail: "31.48",
      handling: "15",
      id: "104",
      productName: "Terramycin Ophthalmic Ointment for Dogs, Cats & Horses",
      subTotal: "46.48",
      img:Product5
    }
  ];
  // const [productData, setProductData] = useState([]);

  // useEffect(() => {
  //   axios
  //     .get("http://localhost:8081/product/getAllProducts")
  //     .then(({ data }) => {
  //       console.log(data);
  //       if (data) {
  //         setProductData(data);
  //       }
  //     })
  //     .catch((error) => {
  //       console.log(error);
  //     });
  // }, []);
  return (
    <Router>
      <Routes>
        <Route exact path="/" element={<Login />} />
        <Route
          exact
          path="/home"
          element={<Products prductDetails={productDetails} />}
        />
        <Route
          exact
          path="/cart"
          element={<Cart prductDetails={productDetails} />}
        />
        <Route
          exact
          path="/checkout/:productIds"
          element={<Checkout prductDetails={productDetails} />}
        />
        <Route
          exact
          path="/cart/:productIds"
          element={<Cart prductDetails={productDetails} />}
        />
        <Route
          exact
          path="/order"
          element={<OrderPlace prductDetails={productDetails} />}
        />
        <Route exact path="/History/:userId" element={<History />} />
      </Routes>
    </Router>
  );
};

export default App;
