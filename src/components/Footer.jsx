import React from "react";

const Footer = () => {
  return (
    <>
      <footer class="footer_section">
        <div class="container">
          <div class="footer-info">
            <p>
              &copy; <span id="displayYear"></span> All Rights Reserved By Loyalty Program
            </p>
          </div>
        </div>
      </footer>
    </>
  );
};

export default Footer;
