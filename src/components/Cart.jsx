import React, { useState } from "react";
import { Link, useParams, useLocation } from "react-router-dom";
import "./style.css";
import Header from "./Header";
import Footer from "./Footer";

const Cart = ({ prductDetails }) => {
  const param = useParams();
  const location = useLocation();
  const { points, equivalentAmount, userId } = location?.state;
  const productIds = param.productIds ? param.productIds.split(",") : [];
  const selectedProduct = productIds?.map((id) =>
    prductDetails?.find((el) => el.id === id)
  );
  const [cartItems, setCartItems] = useState(selectedProduct);

  const removeProducts = (value) => {
    let selectedItems = cartItems.filter((item) => item.id !== value);
    setCartItems(selectedItems);
  };


  let itemsID = cartItems && cartItems?.map((item) => item.id);
  console.log("selectedProducts", cartItems);
  return (
    <>
      <Header state={location.state} />
      <section class="shop_section layout_padding">
        <div class="container">
          <div class="heading_container heading_center">
            <h2>Shopping Cart</h2>
          </div>
          <form method="post" action="#">
            <table cellspacing="0" class="shop_table cart">
              <thead>
                <tr>
                  <th class="product-remove">&nbsp;</th>
                  <th class="product-thumbnail">Product</th>
                  <th class="product-name">Product Name</th>
                  <th class="product-price">Price</th>
                  <th class="product-quantity">Shipping Fee</th>
                  <th class="product-subtotal">Total</th>
                </tr>
              </thead>
              <tbody>
                {cartItems?.map((item) => (
                  <tr class="cart_item" key={item.id}>
                    <td class="product-remove">
                      <span
                        title="Remove this item"
                        class="remove"
                        onClick={() => removeProducts(item.id)}
                      >
                        ×
                      </span>
                    </td>

                    <td class="product-thumbnail">
                      <a href="single-product.html">
                        <img
                          width="145"
                          height="145"
                          alt="poster_1_up"
                          class="shop_thumbnail"
                          src={item.img}
                        />
                      </a>
                    </td>

                    <td class="product-name">
                      <a href="single-product.html">{item.productName}</a>
                    </td>

                    <td class="product-price">
                      <span class="amount">${item.clientRetail}</span>
                    </td>
                    <td class="product-price">
                      <span class="amount">${item.handling}</span>
                    </td>
                    <td class="product-subtotal">
                      <span class="amount">${item.subTotal}</span>
                    </td>
                  </tr>
                ))}
                <tr></tr>

                <tr>
                  <td class="actions" colspan="6">
                    

                    {cartItems?.length > 0 && (
                      <Link
                        to={`/checkout/${itemsID}`}
                        state={{
                          userId: userId,
                          points:points,
                          equivalentAmount:equivalentAmount
                        }}
                      >
                        <input
                          type="button"
                          class="button"
                          style={{background: '#3b4a6b' }}
                          value={`Checkout`}
                        />
                      </Link>
                    )}
                  </td>
                </tr>
              </tbody>
            </table>
            
          </form>
          
          
        </div>
        <div style={{ marginBottom: "10rem" }}></div>
      </section>
      <Footer />
    </>
  );
};

export default Cart;
