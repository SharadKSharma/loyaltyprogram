import React, { useEffect, useState } from "react";
import "./style.css";
import { useParams, useLocation } from "react-router-dom";
import axios from "axios";
import Header from "./Header";
import Footer from "./Footer";

const History = () => {
  const param = useParams();
  const { userId } = param;
  const [historyData, setHistoryData] = useState();
  const location = useLocation();

  useEffect(() => {
    axios
      .get(`http://localhost:8081/rewards/points/getPointsHistory/${location.state.userId || userId}`)
      .then(({ data }) => {
        console.log(data);
        if (data) {
          setHistoryData(data);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }, [userId || location.state.userId]);
  return (
    <>
      <Header state={location.state}/>
      <section class="shop_section layout_padding">
        <div class="container">
          <div class="heading_container heading_center">
            <h2>Reward Points History</h2>
          </div>
          <form method="post" action="#">
            <table cellspacing="0" class="shop_table cart">
              <thead>
                <tr>
                <th class="product-name">Order ID</th>
                <th class="product-name">Total Amount</th>
                  <th class="product-name">Reward Points</th>
                  <th class="product-price">Transaction Type</th>
                  <th class="product-quantity">Date</th>
                </tr>
              </thead>
              <tbody>
                {historyData?.map((item) => (
                  <tr class="cart_item" key={item.id}>
                    <td class="product-name">
                      <a href="single-product.html">{item.transactionId.slice(0, 8)}</a>
                    </td>
                    <td class="product-name">
                      <a href="single-product.html">{item.transactionType==='Redeem' ? '' :item.totalAmount}</a>
                    </td>
                    <td class="product-name">
                      <a href="single-product.html">{item.rewardsPoint}</a>
                    </td>

                    <td class="product-price">
                      <span class="amount">{item.transactionType}</span>
                    </td>
                    <td class="product-price">
                      <span class="amount">{item.createdDate.slice(0, 10)}</span>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </form>
        </div>
        <div style={{ marginBottom: "10rem" }}></div>
      </section>
      <Footer />
    </>
  );
};

export default History;
