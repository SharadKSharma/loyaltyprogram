import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import LoginImg from "../img/loginImage.jpg";
import axios from "axios";

import "./style.css";
import Footer from "./Footer";

const Login = () => {
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(false);

  const onChangeValue = (value, name) => {
    if (name === "user") {
      setUserName(value);
      setError(false);
    } else {
      setPassword(value);
      setError(false);
    }
  };
  const navigate = useNavigate();

  const login = () => {
    axios
      .post(
        "http://localhost:8081/user/login",

        { userName: userName, password: password }
      )
      .then(({ data }) => {
        console.log(data);
        if (data?.id) {
          navigate(`/home`, {
            state: {
              points: data.points,
              equivalentAmount: data.equivalentAmount,
              userId: data.id,
            },
          });
        } else {
          setError(true);
        }
      })
      .catch((error) => {
        console.log(error);
        setError(true);
      });
  };
  return (
    <>
      <section class="about_section layout_padding" style={{ height: "40rem" }}>
        <div class="container  ">
          <div class="row">
            <div class="col-md-6 col-lg-5 ">
              <div class="img-box">
                <img src={LoginImg} alt="" />
              </div>
            </div>
            <div class="col-md-6 col-lg-7">
              <div class="detail-box" style={{ marginTop: "4rem" }}>
                <div class="heading_container">
                  <h2>Login for Loyalty Program</h2>
                </div>
                <p>
                  Enter valid user name and password.
                </p>
                <div style={{ marginTop: "2rem" }}>
                  <input
                    type="text"
                    value={userName}
                    placeholder="User Name"
                    id="user_name"
                    name="User Name"
                    onChange={(e) => onChangeValue(e.target.value, "user")}
                  />
                  <br />
                  <br />
                  <input
                    type="password"
                    value={password}
                    placeholder="Password"
                    id="password"
                    name="password"
                    onChange={(e) => onChangeValue(e.target.value, "pass")}
                  />
                  <br />

                  <span onClick={() => login()}>Login</span>
                  {error && (
                    <p style={{ color: "red" }}>
                      * Please enter valid credintials.
                    </p>
                  )}
                </div>
              </div>
              <div style={{ marginBottom: "10rem" }}></div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default Login;
