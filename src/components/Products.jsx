import React, { useState } from "react";
import { Link, useLocation } from "react-router-dom";
import Footer from "./Footer";
import Header from "./Header";

const Products = ({ prductDetails }) => {
  const [cartProducts, setCartProducts] = useState([]);
  const location = useLocation();
  // const { points, equivalentAmount } = location.state

  const addCart = (item) => {
    setCartProducts((arr) => [...arr, item]);
  };

  const removeItem = (value) => {
    let products = cartProducts.filter((item) => item !== value);
    setCartProducts(products);
  };

  return (
    <>
      <Header state={location.state} />
      <section className="shop_section layout_padding">
        <div className="container">
          <div className="heading_container heading_center">
            <h2>Products List</h2>
          </div>

          <div className="row">
            {prductDetails?.map((product) => (
              <div className="col-sm-6 col-xl-3" key={product.id}>
                <div className="box" style={{ border: "1px solid" }}>
                  <div className="img-box">
                    <img src={product.img} alt="" />
                  </div>
                  <div className="detail-box">
                    <div class="row">
                      <h6
                        style={{
                          width: "50%",
                          left: "1rem",
                          position: "relative",
                        }}
                      >
                        {product.productName}
                      </h6>
                      <h6
                        style={{
                          right: "1rem",
                          position: "absolute",
                        }}
                      >
                        Price:
                        <span style={{ marginLeft: "4px" }}>
                          ${product.clientRetail}
                        </span>
                      </h6>
                    </div>
                  </div>
                  {cartProducts?.includes(product.id) ? (
                    <div
                      className="new"
                      style={{ backgroundColor: "#3b4a6b" }}
                      onClick={() => removeItem(product.id)}
                    >
                      <span>Remove from cart</span>
                    </div>
                  ) : (
                    <div
                      className="new"
                      style={{ backgroundColor: "#3b4a6b" }}
                      onClick={() => addCart(product.id)}
                    >
                      <span>Add to cart</span>
                    </div>
                  )}
                </div>
              </div>
            ))}
          </div>
          {/* <span>{`Cart Items: ${cartProducts.length}`}</span> */}
          {cartProducts.length > 0 && (
            <Link
              to={`/cart/${cartProducts}`}
              state={location.state}
              style={{ textDecoration: "none" }}
            >
              <div class="btn-box" style={{ marginBottom: "8rem" }}>
                <span
                  style={{
                    backgroundColor: "#3b4a6b",
                    border: "1px solid #3b4a6b",
                  }}
                >{`Proceed To Cart (${cartProducts.length})`}</span>
              </div>
            </Link>
          )}
        </div>
        <div style={{ marginBottom: "8rem" }}></div>
      </section>
      <Footer />
    </>
  );
};

export default Products;
