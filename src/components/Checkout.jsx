import React, { useState } from "react";
import "./style.css";
import { useParams, useLocation, useNavigate } from "react-router-dom";
import axios from "axios";
import Header from "./Header";
import Footer from "./Footer";
import ImgRedeem from "../img/redeemImg.jpg";

const Checkout = ({ prductDetails }) => {
  const param = useParams();
  const location = useLocation();
  const navigate = useNavigate();
  const { userId, points, equivalentAmount } = location.state;
  const productIds = param.productIds ? param.productIds.split(",") : [];
  const selectedProduct = productIds?.map((id) =>
    prductDetails?.find((el) => el.id === id)
  );

  // eslint-disable-next-line no-unused-vars
  const [cartItems, _] = useState(selectedProduct);
  const [equivalentDiscount, setEquivalentDiscount] = useState();
  const [redeemPoints, setRedeemPoints] = useState();
  const [desirePoints, setDesirePoints] = useState(0);
  const [errorMsg, setErrorMsg] = useState();
  const [firstName, setFirstName] = useState("Oliver");
  const [lastName, setLastName] = useState("Greens");
  const [state, setState] = useState("California");
  const [address, setAddress] = useState("2719 Center Avenue");
  const [phone, setPhone] = useState("559-366-6671");
  const [city, setCity] = useState("Fresno");
  const [pointsAmmount, setPointsAmmount] = useState(equivalentAmount);
  const [updatedPoints, setUpdatedPoints] = useState(points);
  const sum = cartItems
    .map((item) => parseFloat(item.subTotal))
    .reduce((partialSum, a) => partialSum + a, 0)
    .toFixed(2);

  const placedOrder = () => {
    if (desirePoints) {
      axios
        .put(
          `http://localhost:8081/rewards/points/redeemPoint/${userId}/${desirePoints}`
        )
        .then(({ data }) => {
          console.log(data);
          if (data.equivalentAmount) {
            setEquivalentDiscount(data.equivalentAmount);
          }
        })
        .then(() => {
          axios
            .post(`http://localhost:8081/rewards/points/create/${userId}`, {
              description: "product purchase",
              totalAmount: sum,
            })
            .then(({ data }) => {
              console.log(data);
              if (data?.points) {
                navigate(`/order`, {
                  state: {
                    points: data.points,
                    equivalentAmount: data.equivalentAmount,
                    userId: userId,
                    transactionId: data.transactionId,
                  },
                });
              }
            })
            .catch((error) => {
              console.log(error);
            });
        });
    } else {
      axios
        .post(`http://localhost:8081/rewards/points/create/${userId}`, {
          description: "product purchase",
          totalAmount: sum,
        })
        .then(({ data }) => {
          console.log(data);
          if (data?.points) {
            navigate(`/order`, {
              state: {
                points: data.points,
                equivalentAmount: data.equivalentAmount,
                userId: userId,
              },
            });
          }
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };

  const userRedeemPoints = () => {
    if (redeemPoints / 10 > sum) {
      setErrorMsg(
        `You can avail maximun ${parseInt(sum * 10)} points for this order.`
      );
    } else {
      setEquivalentDiscount((redeemPoints / 10).toFixed(2));
      setPointsAmmount((equivalentAmount - redeemPoints / 10).toFixed(2));
      setUpdatedPoints(points - redeemPoints);
      setRedeemPoints("");
      setErrorMsg("");
    }
  };

  return (
    <>
      <Header state={location.state} />
      <section
        class="shop_section layout_padding"
        style={{ marginBottom: "10rem" }}
      >
        <div class="container">
          <div class="heading_container heading_center">
            <h2>Checkout Details</h2>
          </div>
          <form
            enctype="multipart/form-data"
            action="#"
            class="checkout"
            method="post"
            name="checkout"
          >
            <div className="row">
              <div style={{ width: "60%" }}>
                <h3 id="order_review_heading">Your Products</h3>

                <table class="shop_table" style={{ lineHeight: "1.85" }}>
                  <thead>
                    <tr>
                      <th class="product-name">Product</th>
                      <th class="product-total">Total</th>
                    </tr>
                  </thead>
                  <tbody>
                    {cartItems.map((item) => (
                      <tr class="cart_item">
                        <td class="product-name">{item.productName} </td>
                        <td class="product-total">
                          <span class="amount">${item.subTotal}</span>{" "}
                        </td>
                      </tr>
                    ))}
                  </tbody>
                  <tfoot>
                    <tr class="cart-subtotal">
                      <th>Cart Subtotal</th>
                      <td>
                        <span class="amount">${sum}</span>
                      </td>
                    </tr>
                    {equivalentDiscount && (
                      <tr class="shipping">
                        <th>Redeem Points Discount</th>
                        <td>
                          ${equivalentDiscount || 0}
                          <input
                            type="hidden"
                            class="shipping_method"
                            value="free_shipping"
                            id="shipping_method_0"
                            data-index="0"
                            name="shipping_method[0]"
                          />
                        </td>
                      </tr>
                    )}

                    <tr class="order-total">
                      <th>Order Total</th>
                      <td>
                        <strong>
                          <span class="amount">
                            ${(sum - (equivalentDiscount || 0)).toFixed(2)}
                          </span>
                        </strong>{" "}
                      </td>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <div style={{ marginLeft: "1rem", width: "38%" }}>
                <div style={{ marginBottom: "1rem" }}>
                  <h3 id="order_review_heading">Redeem Reward Points</h3>
                  <div class="create-account">
                    <div style={{ width: "26rem" }}>
                      <img
                        style={{ height: "7rem", width: "26rem" }}
                        src={ImgRedeem}
                        alt="redeem"
                      />
                    </div>
                    <div style={{ marginLeft: "1rem" }}>
                      <div className="row" style={{ marginTop: "10px" }}>
                        <div className="point_redeemtion">
                          <span>
                            Total Reward Points <br /> <h3>{updatedPoints}</h3>
                          </span>
                        </div>
                        <div className="point_redeemtion">
                          <span>
                            Points Worth <br /> <h3>${pointsAmmount}</h3>
                          </span>
                        </div>
                      </div>
                      <div style={{ paddingTop: "2px", paddingBottom: "6px" }}>
                        <span>
                          * Minimum 10 reward points can be redeemed at a time
                        </span>
                      </div>
                      <div class="clear"></div>

                      <input
                        type="text"
                        value={redeemPoints}
                        placeholder="Enter points to redeem"
                        id="redeem"
                        name="redeem_points"
                        class="input-text"
                        onChange={(e) => {
                          setRedeemPoints(e.target.value);
                          setDesirePoints(e.target.value);
                        }}
                        style={{ borderRadius: "10px" }}
                      />
                      <input
                        type="button"
                        class="button"
                        style={{
                          borderRadius: "10px",
                          marginLeft: "5px",
                          background: `${
                            redeemPoints < 10 ? "#3b4a6b70" : "#3b4a6b"
                          }`,
                        }}
                        value={`Redeem`}
                        disabled={redeemPoints < 10}
                        onClick={() => userRedeemPoints()}
                      />
                      <br />
                      {!errorMsg && equivalentDiscount && (
                        <p
                          style={{
                            fontWeight: "bold",
                            fontSize: "18px",
                            marginTop: "0.5rem",
                          }}
                        >
                          Congratulations!! you got ${equivalentDiscount}{" "}
                          discount.
                        </p>
                      )}
                      {errorMsg && (
                        <p style={{ marginTop: "4px" }}>{errorMsg}</p>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div id="customer_details" class="col2-set">
              <div class="col-1" style={{ maxWidth: "98%" }}>
                <div class="woocommerce-billing-fields">
                  <h3 style={{ marginLeft: "-8px" }}>
                    Billing/Shipping Details
                  </h3>
                  <p
                    id="billing_country_field"
                    class="form-row form-row-wide address-field update_totals_on_change validate-required woocommerce-validated"
                  >
                    <label class="" for="billing_country">
                      Country{" "}
                      <abbr title="required" class="required">
                        *
                      </abbr>
                    </label>
                    <select
                      class="country_to_state country_select"
                      id="billing_country"
                      name="billing_country"
                      style={{ border: "1px solid #9d9797" }}
                    >
                      <option value="CA">Canada</option>
                      <option value="FI">Finland</option>
                      <option value="FR">France</option>
                      <option value="IN">India</option>
                      <option value="GB">United Kingdom (UK)</option>
                      <option selected="selected" value="US">
                        United States (US)
                      </option>
                    </select>
                  </p>

                  <div
                    id="billing_first_name_field"
                    class="form-row form-row-first validate-required"
                    style={{ marginTop: "-1rem" }}
                  >
                    <div style={{ width: "48%" }}>
                      <label class="" for="billing_first_name">
                        First Name{" "}
                        <abbr title="required" class="required">
                          *
                        </abbr>
                      </label>
                      <input
                        type="text"
                        value={firstName}
                        placeholder=""
                        id="billing_first_name"
                        name="billing_first_name"
                        class="input-text "
                        onChange={(e) => setFirstName(e.target.value)}
                        style={{ border: "1px solid #9d9797" }}
                      />
                    </div>

                    <div style={{ marginLeft: "2.5rem", width: "48%" }}>
                      <label class="" for="billing_last_name">
                        Last Name{" "}
                        <abbr title="required" class="required">
                          *
                        </abbr>
                      </label>
                      <input
                        type="text"
                        value={lastName}
                        placeholder=""
                        id="billing_last_name"
                        name="billing_last_name"
                        class="input-text "
                        onChange={(e) => setLastName(e.target.value)}
                        style={{ border: "1px solid #9d9797" }}
                      />
                    </div>
                  </div>
                  <div class="clear"></div>

                  <div
                    id="billing_address_1_field"
                    class="form-row form-row-wide address-field validate-required"
                  >
                    <div style={{ width: "48%" }}>
                      <label class="" for="billing_address_1">
                        Address{" "}
                        <abbr title="required" class="required">
                          *
                        </abbr>
                      </label>
                      <input
                        type="text"
                        value={address}
                        placeholder="Street address"
                        id="billing_address_1"
                        name="billing_address_1"
                        class="input-text "
                        onChange={(e) => setAddress(e.target.value)}
                        style={{ border: "1px solid #9d9797" }}
                      />
                    </div>

                    <div style={{ marginLeft: "2.5rem", width: "48%" }}>
                      <label class="" for="billing_city">
                        City{" "}
                        <abbr title="required" class="required">
                          *
                        </abbr>
                      </label>
                      <input
                        type="text"
                        onChange={(e) => setCity(e.target.value)}
                        value={city}
                        placeholder="Town / City"
                        id="billing_city"
                        name="billing_city"
                        class="input-text "
                        style={{ border: "1px solid #9d9797" }}
                      />
                    </div>
                  </div>

                  <div class="clear"></div>

                  <div
                    id="billing_email_field"
                    class="form-row form-row-first validate-required validate-email"
                  >
                    <div style={{ width: "48%" }}>
                      <label class="" for="billing_email">
                        State{" "}
                        <abbr title="required" class="required">
                          *
                        </abbr>
                      </label>
                      <input
                        type="text"
                        value={state}
                        placeholder="State"
                        id="billing_email"
                        name="billing_email"
                        class="input-text "
                        onChange={(e) => setState(e.target.value)}
                        style={{ border: "1px solid #9d9797" }}
                      />
                    </div>

                    <div style={{ marginLeft: "2.5rem", width: "48%" }}>
                      <label class="" for="billing_phone">
                        Phone{" "}
                        <abbr title="required" class="required">
                          *
                        </abbr>
                      </label>
                      <input
                        type="text"
                        value={phone}
                        placeholder=""
                        id="billing_phone"
                        name="billing_phone"
                        class="input-text "
                        onChange={(e) => setPhone(e.target.value)}
                        style={{ border: "1px solid #9d9797" }}
                      />
                    </div>
                  </div>
                  <div class="clear"></div>
                </div>
              </div>
            </div>

            <div id="order_review" style={{ position: "relative" }}>
              <h3
                style={{
                  color: "#3b4a6b",
                  fontSize: "20px",
                  textTransform: "uppercase",
                  marginTop: "3rem",
                }}
              >
                Payment Details
              </h3>
              <div id="payment">
                <ul class="payment_methods methods">
                  <li class="payment_method_paypal">
                    <input
                      type="radio"
                      data-order_button_text="Proceed to PayPal"
                      value="paypal"
                      name="payment_method"
                      class="input-radio"
                      id="payment_method_paypal"
                    />
                    <label
                      for="payment_method_paypal"
                      style={{ marginLeft: "4px" }}
                    >
                      PayPal{" "}
                      <img
                        alt="PayPal Acceptance Mark"
                        src="https://www.paypalobjects.com/webstatic/mktg/Logo/AM_mc_vs_ms_ae_UK.png"
                      />
                    </label>
                    <div
                      style={{ display: "none" }}
                      class="payment_box payment_method_paypal"
                    >
                      <p>
                        Pay via PayPal; you can pay with your credit card if you
                        don’t have a PayPal account.
                      </p>
                    </div>
                  </li>
                </ul>

                <div class="form-row place-order">
                  <input
                    style={{ marginLeft: "43%" }}
                    type="button"
                    data-value="Place order"
                    value="Place order"
                    id="place_order"
                    name="woocommerce_checkout_place_order"
                    class="button alt"
                    onClick={() => placedOrder()}
                  />
                </div>

                <div class="clear"></div>
              </div>
            </div>
          </form>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default Checkout;
