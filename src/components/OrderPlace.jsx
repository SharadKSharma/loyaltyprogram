import React, { useEffect, useState } from "react";
import OrderPlaced from "../img/orderPlaced.png";
import "./style.css";
import { useLocation } from "react-router-dom";
import axios from "axios";
import Header from "./Header";
import Footer from "./Footer";

const OrderPlace = () => {
  const location = useLocation();
  const { equivalentAmount, userId, points, transactionId } = location.state;
  const [rewardPointsData, setRewardPointsData] = useState();

  useEffect(() => {
    axios
      .get(`http://localhost:8081/rewards/points/getRewardsPoints/${userId}`)
      .then(({ data }) => {
        console.log(data);
        if (data) {
          setRewardPointsData(data);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }, [userId]);
  return (
    <>
      <Header state={{equivalentAmount: rewardPointsData?.equivalentAmount, userId:location.state.userId, points:rewardPointsData?.points}}/>
      <section class="about_section layout_padding" style={{ height: "35rem" }}>
        <div class="container  ">
          <div class="row">
            <div class="col-md-6 col-lg-5 ">
              <div class="img-box">
                <img src={OrderPlaced} alt="" />
              </div>
            </div>
            <div class="col-md-6 col-lg-7" style={{ marginTop: "2rem" }}>
              <div class="detail-box">
                <div class="heading_container">
                  <h2>Thank you for your order!</h2>
                </div>
                <p style={{fontSize: '20px'}}>Your order has been placed.</p>
                <p style={{fontSize: '20px'}}>Your order Id : {transactionId?.slice(0, 8)}</p>
                <p style={{ display: "flex" }}>
                  <span>
                    <h1 style={{fontSize: '2.3rem'}}>Congratulations!!</h1>
                      You have won {points} reward points.
                  </span>
                </p>
                <div class="heading_container">
                  <h2>Total Reward Points</h2>
                </div>
                <div style={{ marginLeft: "2rem" }}>
                  <div className="row">
                    <div>
                      <span style={{ width: "12rem", height: "8rem" }}>
                        Reward Points <br />{" "}
                        <h1>{rewardPointsData?.points}</h1>
                      </span>
                    </div>
                    <div>
                      <span
                        style={{
                          width: "12rem",
                          height: "8rem",
                          marginLeft: "2rem",
                        }}
                      >
                        Points Worth <br />{" "}
                        <h1>${rewardPointsData?.equivalentAmount}</h1>
                      </span>
                    </div>
                  </div>

                  <div class="clear"></div>
                </div>
              </div>
            </div>
          </div>
          <br />
          <br />
        </div>
      </section>
      <Footer />
    </>
  );
};

export default OrderPlace;
