import React from "react";
import { Link } from "react-router-dom";

const Header = ({state}) => {

  console.log('state', state)
  return (
    <div class="hero_area">
      <header class="header_section">
        <div class="container-fluid">
          <nav class="navbar navbar-expand-lg custom_nav-container ">
            <Link to={`/home`} className="navbar-brand">
              <span style={{fontSize: '28px'}}>LOYALTY PROGRAM</span>
            </Link>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav" style={{right: '5rem', position: 'absolute'}}>
                <li class="nav-item">
                  <Link to={`/home`} className="nav-link" state={state}>
                    Home
                  </Link>
                </li>
                <li class="nav-item">
                  <Link to={`/history/${state?.userId}`} className="nav-link" state={state}>
                    History
                  </Link>
                </li>
                <li class="nav-item">
                  <Link to={`/`} className="nav-link">
                    Logout
                  </Link>
                </li>
              </ul>
            </div>
          </nav>
        </div>
      </header>
    </div>
  );
};

export default Header;
